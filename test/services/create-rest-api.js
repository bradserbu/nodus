'use strict';

// ** Dependencies
const program = require('nodus-framework').program;
const restApiService = require('../../services/interfaces/rest-api');

function sayhello(name) {
    return `Hello, ${name}!`;
}

function throwError() {
    throw Error('TEST_ERROR');
}

// ** Exports
function createService() {
    return restApiService('api', {
        endpoints: [
            {
                "method": "GET",
                "path": "/sayhello",
                "handler": sayhello
            },
            {
                "method": "GET",
                "path": "/error",
                "handler": throwError
            }
        ]
    });
}

// Start the api service
createService()
    .tap(service => {
        service.on('error', err => {
            console.error('******** SERVICE ERROR ********', err);
        })
    })
    .then(service => service.start())
    .then(service => {
        program.on('shutdown', () => service.stop());
    });