'use strict';

const DEFAULT_SERVER = 'localhost';
const DEFAULT_PORT = 8080;
const DEFAULT_PATH = '';

// ** Dependencies
const Path = require('path');
const program = require('nodus-framework').program;
const logger = require('nodus-framework').logging.createLogger("web-socket-client");
const WebSocket = require('ws');

function openWebSocket(server, port, path) {

    server = server || DEFAULT_SERVER;
    port = port || DEFAULT_PORT;
    path = path || DEFAULT_PATH;

    return new Promise((resolve, reject) => {

        //const ws = new WebSocket(`wss://${server}:${port}/${path}`, 'borf'); // TODO: WTF is 'borf'?
        const url = `ws://${server}:${port}/${path}`;
        const ws = new WebSocket(url, 'borf'); // TODO: WTF is 'borf'?

        ws.on('open', function open() {
            logger.info("connected");
        });

        ws.on('close', function close() {
            logger.info('disconnected');
        });

        // Binary Data Listener
        ws.addListener('data', function (buf) {
            logger.info('DATA', buf);
        });

        ws.onmessage = function (m) {
            logger.info('MESSAGE', m);
        };

        // Close websocket on program shutdown
        program.on('shutdown', () => {
            logger.info("Closing WS connection...");
            ws.close();
            resolve();
        });
    });
}

module.exports = openWebSocket;
