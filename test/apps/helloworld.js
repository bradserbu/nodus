'use strict';

// ** Dependencies
const application = require('../../lib/application');
const sayhello = require('../../examples/helloworld/sayhello');
const restApiService = require('../../services/interfaces/rest-api');

// ** Exports
module.exports = application
    .create('helloworld', 'The Nodus "Hello World" application')
    // .command('sayhello', sayhello)
    .service('api', restApiService, {
        endpoints: [
            {
                "method": "GET",
                "path": "/sayhello",
                // "handler": "commands:sayhello"
                "handler": sayhello
            }
        ]
    });