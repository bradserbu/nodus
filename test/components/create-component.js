'use strict';

// ** Components
const components = require('../../lib/components');

module.exports = name => components.create(name);