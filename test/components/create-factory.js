'use strict';

// ** Dependencies
const components = require('../../lib/components');

module.exports = () => {
    const factory = components.createFactory(components.Component);

    return factory('test-component');
};