#!/usr/bin/env bash

## bradserbu/nodus:build
docker build -f Dockerfile-build -t bradserbu/nodus:build . && docker push bradserbu/nodus:build

## bradserbu/nodus - The nodus command line executable
docker build -f Dockerfile-app -t bradserbu/nodus:app . && docker push bradserbu/nodus:app

