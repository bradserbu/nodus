'use strict';

// ** Dependencies
const util = require('util');
const Component = require('./components').Component;
const activities = require('nodus-framework').activities;
const errors = require('nodus-framework').errors;
const services = require('./services');

class Application extends Component {
    constructor(name, options) {
        super(name, options);

        this._commands = {};
        this._services = {};
    }

    command(name, handler, options) {

        this.logger.debug('CREATE_COMMAND', arguments);

        const command = activities(name, handler, options);

        // Forward all command events as application events
        command.onAny((event, data) => {
            this.emit(`command:${name}:${event}`, data);
        });

        // Add command to the application
        this._commands[name] = command;

        return this;
    }

    run(name, args, options) {

        const command = this._commands[name];

        this.logger.debug('RUN_COMMAND', command);

        return command.run(args, options);
    }

    service(name, provider, options) {

        this._services[name] = {
            name: name,
            provider: provider,
            options: options,
            started: false
        };

        return this;
    }

    startService(name) {

        const service = this._services[name];
        if (!service) throw errors('SERVICE_NOT_FOUND', 'The service could not be found', {name: name});

        // Create a new service
        return service
            .provider(name, service.options)
            .then(instance => {

                // Track the instance of this service in the application registry
                service.instance = instance;

                // Forward all service events as application events
                instance.onAny((event, data) => {
                    this.emit(`service:${name}:${event}`, data);
                });

                // Track the status of the services
                instance.on('started', () => service.started = true);
                instance.on('stopped', () => service.started = false);

                return instance.start();
            });

        return this;
    }

    stopService(name) {

        const logger = this.logger;
        const service = this._services[name];

        if (!service) throw errors('SERVICE_NOT_FOUND', 'The service could not be found', {name: name});

        logger.debug('STOP_SERVICE', service);

        // ** Ensure this service is started
        if (!service.started)
            throw errors('SERVICE_STOPPED', 'The service is already stopped.', service);

        // Stop the service
        service.instance.stop();

        return this;
    }

    shutdown() {

        this.emit('shutdown');

        // TODO: Stop all started services

        return this;
    }
}

function createApplication(name, description) {

    const app = new Application(name, {
        description: description
    });

    return app;
}

// ** Exports
module.exports = createApplication;
module.exports.create = createApplication;
module.exports.Application = Application;