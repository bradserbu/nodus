'use strict';

// ** Constants
const DEFAULT_NODUS_DIR = '~/.nodus';

// ** Dependencies
const files = require('nodus-framework').files;
const path = require('path');
const logger = require('nodus-framework').logging.createLogger('nodus:config');

function config(options) {

    options = options || {};
    options.nodus_dir = files.resolvePath(options.nodus_dir || process.env['NODUS_DIR'] || DEFAULT_NODUS_DIR);
    options.apps_dir = path.join(options.nodus_dir, '/apps');
    options.system_dir = path.join(options.nodus_dir, '/sys');
    options.docker_dir = path.join(options.system_dir, '/docker');
    options.docker_file = path.join(options.docker_dir, 'Dockerfile');
    options.docker_ignore = path.join(options.docker_dir, '.dockerignore');

    logger.debug('OPTIONS', options);

    return options;
}

// ** Exports
module.exports = config;