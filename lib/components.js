'use strict';

// ** Dependencies
const _ = require('lodash');
const util = require('util');
const EventEmitter = require('eventemitter2');
const logging = require('nodus-framework').logging;
const errors = require('nodus-framework').errors;
const functions = require('nodus-framework').functions;
const NOOP = require('nodus-framework').noop;

const logger = logging.createLogger('components');

class Component extends EventEmitter {
    constructor(name, options) {
        super();

        this._name = name;
        this._description = options.description || '';
        this._logger = logging.createLogger(`${name}`);
        this._options = options;

        // Log all application events
        this.onAny((event, data) => {

            const event_name = event;
            const event_data = data;

            // Log Event
            event_data
                ? this.logger.debug(`[${event_name}]`, data)
                : this.logger.debug(`[${event_name}]`);
        });
    }

    get name() {
        return this._name;
    }

    get description() {
        return this._description;
    }

    get logger() {
        return this._logger;
    }

    config(name, value) {

        if (arguments.length === 1)
            return this._options[name];

        // TODO: Proxy based data binding
        this._options[name] = value;
    }
}

function createComponent(type, name, options, configure) {

    logger.debug('CREATE_COMPONENT', arguments);

    // // createComponent({name:, ...}) -> createComponent(.name, {...})
    // if (arguments.length === 1 && util.isObject(arguments[0]))
    //     return createComponent(arguments[0].name, arguments[0]);
    //
    // // createComponent(name, {provider:,...}) -> createComponent(name, .provider, {...})
    // if (arguments.length === 2 && util.isObject(arguments[1]))
    //     return createComponent(arguments[0], arguments[1].provider, arguments[1]);

    // createComponent(name, provider, configure) -> createComponent(name, provider, null, configure)
    // if (arguments.length === 3 && util.isObject(arguments[2]))
    //     return createComponent(arguments[0], arguments[1], null, arguments[2]);

    // Type is required
    if (util.isNullOrUndefined(type))
        throw errors('ARGUMENT_REQUIRED', 'The "type" argument is required.', arguments);

    // Name is required
    if (_.isEmpty(name))
        throw errors('ARGUMENT_REQUIRED', 'The "name" argument is required', arguments);

    // TODO: loadProvider -> Pass provider as 'string' -> files.require
    // TODO: loadConfigure -> Pass configure as 'string' -> files.require

    // Argument Defaults
    options = options || {};
    type = type || Component;
    configure = configure || NOOP;

    // Create a new component
    logger.debug('TYPE', type);

    const component = functions.isClass(type)
        ? new type(name, options)
        : type(name, options);

    // Configure if set
    if (configure) {
        component.logger.debug('CONFIGURE', component);
        configure(component);
    }

    // Return the configured component
    return component;
}

function createFactory(type, configure) {

    return (name, options) => {

        return createComponent(type, name, options, configure);

        // const args = Array.prototype.slice.call(arguments);
        // args.unshift(type);
        //
        // // logger.debug('FACTORY_CREATE', args);
        // return createComponent.apply(this, args);
    }
}

// ** Exports
module.exports = createComponent;
module.exports.createFactory = createFactory;
module.exports.Component = Component;