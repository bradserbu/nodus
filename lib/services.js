'use strict';

// ** Dependencies
const _ = require('lodash');
const util = require('util');
const components = require('./components');
const EventEmitter = require('eventemitter2');
const files = require('nodus-framework').files;
const NOOP = require('nodus-framework').noop;
const logging = require('nodus-framework').logging;
//const Action = require('actum').Action;
const Action = require('nodus-framework').actions;
const curry = require('ramda').curry;

function Provider(provider) {
    // Provider is a path to a NodeJS module
    if (util.isString(provider)) {
        provider = files.requireFile(provider);
    }

    // Provider is a function so just use it
    if (!util.isFunction(provider))
        throw errors('ARGUMENT_TYPE_ERROR', 'The provider argument type is not supported.', {provider: provider});

    return provider;
}

function Configuration(service, config) {

    // TODO: Validate Configuration
    return config;
}

function Commands(service, commands) {

    const ret = _.mapValues(commands, (options, name) => {

        const provider = Provider(options.provider);
        const command = Action(name, curry(provider)(service), options); // TODO: Just pass parameters?

        return command;
    });

    return ret;
}

class Service extends EventEmitter {
    constructor(type, name, options) {
        super();

        // Create logger
        this._logger = logging.createLogger(`service:${name}`);

        // Properties
        this._type = type;
        this._name = name;
        this._description = options.description;
        this._configuration = Configuration(this, options.config);
        this._commands = Commands(this, options.commands);

        // Actions
        const init = options.init || NOOP;
        const configure = options.configure || NOOP;
        const start = options.start || NOOP;
        const stop = options.stop || NOOP;

        // Hooks
        this._init = Action('init', () => init(this));
        this._configure = Action('configure', opts => configure(this, opts));
        this._start = Action('start', () => start(this));
        this._stop = Action('stop', () => stop(this));
    }

    get type() {
        return this._type;
    }

    get name() {
        return this._name;
    }

    get description() {
        return this._description;
    }

    get logger() {
        return this._logger;
    }

    command(name) {
        const command = this._commands[name];
        return command;
    }

    config(name, value) {

        // TODO: This is stupid... Move to Configuration() class
        if (arguments.length === 1)
            return this.getConfig(name);

        if (arguments.length === 2)
            return this.setConfig(name, value);
    }

    setConfig(name, value) {
        const property = this._configuration[name];

        this.logger.debug('SET', name, value);
        property.value = value;
    }

    getConfig(name) {
        const property = this._configuration[name];

        const value = property.value || property.defaultValue;

        this.logger.debug('CONFIG', {
            name: name,
            value: value
        });

        return value;
    }

    init(config) {
        const self = this;
        const init = this._init.run;
        const configure = this._configure.run;

        return init(config)
            .then(() => configure(config))
            .then(() => {
                self.emit('initialized');
                return self;
            });
    }

    start() {
        const self = this;
        const start = this._start.run;

        return start()
            .then(() => {
                self.emit('started');
                return self;
            });
    }

    stop() {
        const self = this;
        const stop = this._stop.run;

        return stop()
            .then(() => {
                self.emit('stopped');
                return self;
            });
    }

    toJSON() {

        const command_stats = _.mapValues(this._commands, command => command.stats);

        return {
            type: this.type,
            name: this.name,
            description: this.description,
            stats: {
                commands: command_stats
            }
        }
    }
}

/**
 * Return a function that will create a new service.
 * @param type
 * @param options
 * @returns {service}
 */
function createService(type, options) {

    // Pass back a function that will create a new instance of the service
    const service = function (name, config) {
        const service = new Service(type, name, options);
        return service.init(config);
    };

    // Properties
    service.type = type;

    // Support creating a service via service(name, options) or service.create(name, options)
    service.create = service;

    return service;
}

// ** Exports
module.exports = createService;