#!/usr/bin/env node
'use strict';

// ** Constants
const EMPTY_COMMAND_MESSGE = 'Please specify a command';
const INVALID_COMMAND_MESSAGE = 'Invalid command';

// ** Dependencies
const util = require('util');
const yargs = require('yargs');
const files = require('nodus-framework').files;
const program = require('nodus-framework').program;
const errors = require('nodus-framework').errors;
const logger = require('nodus-framework').logging.createLogger('nodus');

/**
 * Create a service from a specified provider
 * @param provider
 * @param name
 * @param options
 */
function createService(provider, name, options) {

    // Load the service provider
    const service = files.requireFile(provider);

    return service
        .create(name, options)
        .tap(service => {
            // Log all service events
            service.onAny((event, data) => util.isNullOrUndefined(data)
                ? logger.debug(`service:${name}:${event}`)
                : logger.debug(`service:${name}:${event}`, data)
            );
        });
}

// List of supported program commands
// TODO: Move to generic 'program' module
const COMMANDS = {
    start: (argv) => {
        logger.debug('ARGV', argv);

        // Load the service provider
        const provider = argv.service;

        // The name of the service instance
        const name = argv.name;
        logger.debug('NAME', name);

        // Use the rest of the options as the command line options
        const options = argv;
        logger.debug('OPTIONS', options);

        // Create a new instance of the service
        return createService(provider, name, options)
            .tap(service => service.start())
            .tap(service => {
                // Stop the service on program shutdown
                program.on('shutdown', () => {
                    logger.debug('Stopping service...');
                    service.stop();
                });
            });
    }
};

// Program Help
yargs
    .usage('Usage: $0 <command> [options]')
    .command('start <service> <name>', 'Create a new service instance', {
        service: {
            describe: 'The type of service to create.',
            required: true
        },
        name: {
            describe: 'The name of the service instance.',
            required: false
        }
    })
    .epilog('Copyright (c) 2017 Brad Serbu <bradserbu@bradserbu.com>\nAll rights reserved.')
    .version()
    .help();

// Parse CLI Args
const argv = yargs.argv;
logger.debug('ARGV', argv);

// Get the name of the command from the CLI ARGS
const commandName = argv._.shift();
if (!commandName) {
    yargs.showHelp();
    console.log(EMPTY_COMMAND_MESSGE);
    return;
}

// Get the command to run
const command = COMMANDS[commandName.toLowerCase()];
if (!command) {
    console.error(errors('INVALID_COMMAND', INVALID_COMMAND_MESSAGE, commandName));
    return;
}

// Run the command
command(argv);