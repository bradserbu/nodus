'use strict';

// ** Dependencies
const _ = require('lodash');
const files = require('nodus-framework').files;
const Application = require('../lib/application');
const errors = require('nodus-framework').errors;
const logger = require('nodus-framework').logging.createLogger('nodus:app');

function deepMap (obj, cb) {
    var out = {};

    Object.keys(obj).forEach(function (k) {
        var val;

        if (obj[k] !== null && typeof obj[k] === 'object') {
            val = deepMap(obj[k], cb);
        } else {
            val = cb(obj[k], k);
        }

        out[k] = val;
    });

    return out;
}

function loadHandler(application, handler) {

    const splitter = handler.split(':');

    // Provider is a reference to an application command
    if (splitter[0] == 'commands') {

        const commandName = splitter[1];
        const ret = (args) => {
            logger.debug('ARGS', args);
            application.run(commandName, args);
        };

        logger.debug('HANDLER', ret);

        return ret;
    }

    // throw errors('ARGUMENT_ERROR', 'Unable to load handler', handler);
    // ** Require the handler
    return files.requireFile(handler);
}

function loadServiceProvider(application, provider) {
    return require(`../services/${provider}`);
}

function loadApplication(app) {

    const name = app.name;
    const version = app.version;
    const description = app.description;

    // Create new application
    const application = Application
        .create(name, {
            description: description,
            version: version
        });

    // Add Commands
    _.forEach(app.commands, (command, name) => {

        logger.debug('Loading command...', name);

        const provider = files.requireFile(command.provider);
        const parameters = command.parameters;

        application.command(name, provider, {
            parameters: parameters
        });
    });

    // Add Services
    _.forEach(app.services, (service, name) => {
        logger.debug('Loading service...', name);

        const provider = loadServiceProvider(app, service.provider);
        logger.debug('PROVIDER', provider);

        // const options = _.deeply(_.map)(service, (val, key) => {
        //     logger.debug('DEEP_MAP', {
        //         key: key,
        //         value: val
        //     });
        //
        //     if (key === "handler") {
        //         return loadHandler(val);
        //     }
        //
        //     // Not a provider
        //     return val;
        // });

        const options = deepMap(service, function (val, key) {
            logger.debug('DEEP_MAP', {
                key: key,
                value: val
            });

            if (key === "handler") {
                return loadHandler(application, val);
            }

            // Not a provider
            return val;
        });

        logger.debug('SERVICE_OPTIONS', options);

        application.service(name, provider, options);
    });

    return application;
}

// ** Exports
module.exports = loadApplication;