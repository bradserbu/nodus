#!/usr/bin/env node
'use strict';

// ** Dependencies
const yargs = require('yargs');

// Program Help
yargs
    .usage('Usage: $0 <command>')
    .commandDir('./cmds')
    .version()
    .help()
    .demandCommand(1, '*** Please enter a command ***')
    .epilog('Copyright (c) 2017 Brad Serbu <bradserbu@bradserbu.com>\nAll rights reserved.');

// Parse CLI Arguments
const argv = yargs.argv;

// Empty command
const command = argv._.shift();
if (!command) {
    return yargs
        .showHelp();
}