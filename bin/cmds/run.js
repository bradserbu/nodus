'use strict';

// ** Dependencies
const files = require('nodus-framework').files;
const App = require('../app');

function runCommand(argv) {
    const app = argv.app;
    const cmd = argv.cmd;

    // TODO: Separate options from args
    const args = argv;
    const options = argv;

    // Load the application
    const appJson = files.requireFile(app);

    // Load the application from the json definition
    const application = App(appJson);

    // Run an application command
    return application
        .run(cmd, args, options)
        .then(result => console.log(result))
        .catch(err => console.error(err))
        .finally(() => application.shutdown());
};

// ** Exports
module.exports.command = 'run <cmd> [options]';
module.exports.desc = 'Run an application command';
module.exports.handler = runCommand;
module.exports.builder = yargs => yargs
    .option('app', {
        describe: 'The application containing the command.',
        required: true,
        default: 'app.json'
    })
    .option('cmd', {
        describe: 'The command to run.',
        required: true
    });