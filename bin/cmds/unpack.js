'use strict';

const NODUS_DIR = '~/.nodus';
const DEFAULT_APP_JSON = './app.json';

// ** Dependencies
const Promise = require('bluebird');
const Action = require('nodus-framework').actions;
const files = require('nodus-framework').files;
const path = require('path');
const read = require('fs').createReadStream;
const unpack = require('tar-pack').unpack;
const config = require('../../lib/config');
const logger = require('nodus-framework').logging.createLogger('nodus:pack');

const unpackApplication = Action('unpack-application', options => {

    // Store the current working directory for which the 'nodus' command was run
    const CWD = process.cwd();

    options = options || {};

    // Load Nodus Configuration
    const nodus_config = config();

    // Load app.json
    const appPath = files.resolvePath(options.app || DEFAULT_APP_JSON);

    // Determine what directory to package
    const inputDir = files.dirname(appPath);
    logger.debug('INPUT_DIR', inputDir);

    // Change directory to the location of the app.json file
    logger.debug('Changing directory to app dir...', inputDir);
    process.chdir(inputDir);

    // Load the application json
    logger.debug('Loading application definitions...', appPath);
    const app = files.requireFile(appPath);
    logger.debug('APP', app);

    // TODO: Ensure these properties have a value
    const appName = app.name;
    const appVersion = app.version;

    // TODO: Read the name, version of the package from the app.json
    // const packageName = options.package || 'package.tar.gz';
    const packageName = `${appName}-${appVersion}.package.tar.gz`;
    logger.debug('PACKAGE_NAME', packageName);

    // Determine where to write the package file to
    // const outputDir = files.resolvePath(options.output_dir || process.cwd());
    const appsDir = nodus_config.apps_dir;
    logger.debug('APPS_DIR', appsDir);

    // Package the files in the current working directory
    // TODO: Support logger.info -> write to stdout
    logger.debug('Unpacking application...');
    return new Promise((resolve, reject) => {
        const filepath = files.resolvePath(path.join(appsDir, packageName));

        const destination = path.join(appsDir, `./${appName}-${appVersion}/`);
        logger.debug('Unpacking application...', {
            from: filepath,
            to: destination
        });

        read(filepath)
            .pipe(unpack(destination, err => {
                if (err) return reject(err);

                logger.debug("**** UNPACK FINISHED ****");
                // console.log(result);
                resolve({
                    success: true,
                    result: destination
                });
            }));
    });
});

function packageCommand(argv) {
    return unpackApplication.run(argv);
}

// ** Exports
module.exports.command = 'unpack [options]';
module.exports.desc = 'Unpack an application.';
module.exports.handler = packageCommand;