'use strict';
/**
 * Command to package and build your Nodus Application.
 */

// ** Constants
const DEFAULT_APP_JSON = './app.json';
const DOCKER_COMPOSE_VERSION = '2';

// ** Dependencies
const _ = require('lodash');
const $ = require('highland');
const extend = require('extend');
const Promise = require('bluebird');
const fs = require('fs-extra');
const Action = require('nodus-framework').actions;
const files = require('nodus-framework').files;
const path = require('path');
const logger = require('nodus-framework').logging.createLogger('nodus:build');
const config = require('../../lib/config');
const execa = require('execa');
const yamljs = require('yamljs');

function getDockerServices(app) {

    const containerServices = {};

    _.each(app.services, (service, name) => {
        logger.debug('DOCKER_SERVICE_CONFIG', name);

        const baseConfig = {
            build: '.',
            command: `start ${name}`,
            environment: ["DEBUG=$DEBUG"]
        };

        // TODO: Load balancer support
        const serviceConfig = service.container;

        // Add an HTTP Load Balancer
        if (serviceConfig.loadBalance) {
            logger.debug("**** Using HTTP LOAD BALANCER ****");

            // ** Use standard service configuration
            const config = extend(true, {}, baseConfig, {
                ports: serviceConfig.ports,
                environment: serviceConfig.environment
            });

            containerServices[name] = config;

            // Add Load Balancer
            const lb_name = `lb_${name}`;
            const lb_config = {
                image: "dockercloud/haproxy",
                ports: _.map(serviceConfig.ports, port => `${port}:80`),
                links: [name],
                volumes: ["/var/run/docker.sock:/var/run/docker.sock"]
            };

            containerServices[lb_name] = lb_config;
        } else {
            // ** Use standard service configuration (Expose Ports)
            const config = extend(true, {}, baseConfig, {
                ports: _.map(serviceConfig.ports, port => `${port}:${port}`),
                environment: serviceConfig.environment
            });

            containerServices[name] = config;
        }
    });

    return containerServices;
}

const buildApplication = Action('build-application', options => {

    // Store the current working directory for which the 'nodus' command was run
    const CWD = process.cwd();

    options = options || {};

    // Load Nodus Configuration
    const nodus_config = config();

    // Load app.json
    const appPath = files.resolvePath(options.app || DEFAULT_APP_JSON);

    // Determine what directory to package
    const inputDir = files.dirname(appPath);
    logger.debug('INPUT_DIR', inputDir);

    // Change directory to the location of the app.json file
    logger.debug('Changing directory to app dir...', inputDir);
    process.chdir(inputDir);

    // Load the application json
    logger.debug('Loading application definitions...', appPath);
    const app = files.requireFile(appPath);

    const appName = app.name;
    const appVersion = app.version;

    // Navigate to the build directory
    const buildDir = path.join(nodus_config.apps_dir, `${appName}-${appVersion}`);
    logger.debug('Changing to build directory...', buildDir);
    process.chdir(buildDir);

    // Inject Dockerfile
    logger.debug('Injecting dockerfile...', nodus_config.docker_file);
    fs.copySync(nodus_config.docker_file, 'Dockerfile');

    // Copy Docker ignore file

    // TODO: Using docker-compose in favor of docker for packaging...
    function buildDockerImage() {
        // Build docker image
        const tag = `nodus/${appName}:${appVersion}`;
        logger.debug("Building docker image...", tag);

        return execa('docker', ['build', '-t', tag, '-q', '.'])
            .then(result => {
                logger.debug('RESULT', result);

                // Remove the 'sha256:' prefix
                const container_id = result.stdout.replace('sha256:', '');
                logger.debug('CONTAINER_ID:', container_id);

                return {
                    container_id: container_id
                };
            });
    }

    function buildDockerCompose() {
        logger.debug("Generating container configuration...");
        const docker_config_json = {
            version: DOCKER_COMPOSE_VERSION,
            services: getDockerServices(app)
        };

        logger.debug("Generating YAML file...");
        const docker_compose_yaml = yamljs.stringify(docker_config_json, 10);

        logger.debug("Writing docker-compose file...");
        logger.debug("------------------");
        logger.debug(docker_compose_yaml);
        logger.debug("------------------");

        return Promise.resolve(fs
            .writeFile('docker-compose.yml', docker_compose_yaml))
            .then(() => {
                const proc = execa('docker-compose', ['build'])
                    .catch(err => {
                        if (err.signal === 'SIGINT')
                            return logger.debug('Docker event monitor stopped via SIGINT');

                        // // Unknown error occurred
                        // logger.debug('UNKNOWN_ERROR', 'The docker command failed for an unknown reason', err);
                    });

                // Forward output to the logger
                $(proc.stdout)
                    .split()
                    .each(line => logger.debug('\t', line))
                    .done(() => logger.debug("***** DONE *****"));

                // Pipe docker-compose log to logger
                return proc;
            });
    }

    return buildDockerCompose();
});

function buildCommand(argv) {
    return buildApplication.run(argv);
}

// ** Exports
module.exports.command = 'build [options]';
module.exports.desc = 'Build an Application.';
module.exports.handler = buildCommand;