'use strict';

// ** Dependencies
const _ = require('lodash');
const logging = require('nodus-framework').logging;
const files = require('nodus-framework').files;
const program = require('nodus-framework').program;
const App = require('../app');
const logger = logging.createLogger('nodus:start');

function startService(argv) {
    logger.debug('ARGV', argv);

    const app = argv.app;
    const service = argv.service;

    // TODO: Separate options from args
    const options = argv;

    // Load the application json file
    const appJson = files.requireFile(app);
    logger.debug('APPLICATION', appJson);

    // Load the application
    const application = App(appJson);

    program.on('shutdown', () => {
        application.stopService(service);
    });

    return application
        .startService(service);
}

// ** Exports
module.exports.command = 'start <service> [options]';
module.exports.desc = 'Start an application service';
module.exports.handler = startService;
module.exports.builder = yargs => yargs
    .option('app', {
        describe: 'The application containing the command.',
        required: true,
        default: 'app.json'
    })
    .option('service', {
        describe: 'The command to run.',
        required: false
    });
