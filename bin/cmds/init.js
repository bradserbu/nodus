'use strict';

// ** Constants
const DEFAULT_NODUS_DIR = '~/.nodus';

// ** Dependencies
const fs = require('fs-extra');
const Process = require('nodus-framework').processes;
const logger = require('nodus-framework').logging.createLogger('nodus:init');
const files = require('nodus-framework').files;
const path = require('path');
const config = require('../../lib/config');

const initProcess = Process('nodus-init', 'Initialize the nodus runtime environment.')
    .then('load-config', options => config(options))
    .tap('create-nodus-dir', options => {
        logger.debug('Creating nodus directory...', options.nodus_dir);
        return fs.ensureDirSync(options.nodus_dir);
    })
    .tap('create-apps-dir', options => {
        logger.debug('Creating nodus apps directory...', options.apps_dir);
        return fs.ensureDirSync(options.apps_dir);
    })
    .tap('create-system-dir', options => {
        logger.debug('Creating nodus system directory...', options.system_dir);
        return fs.ensureDir(options.system_dir);
    })
    .tap('create-system-docker-dir', options => {
        logger.debug('Creating nodus docker directory...', options.docker_dir);
        return fs.ensureDirSync(options.docker_dir);
    })
    .tap('load-docker-files', options => {
        logger.debug('Loading docker files into system directory...', options.docker_dir);

        const dockerFile = path.join(__dirname, '../../Dockerfile');
        const dockerIgnore = path.join(__dirname, '../../.dockerignore');
        const directory = options.docker_dir;

        return files.copyFiles(directory, [dockerFile, dockerIgnore]);
    });

function initCommand(argv) {
    return initProcess.run(argv);
}

// ** Exports
module.exports.command = 'init [options]';
module.exports.desc = 'Initialize the nodus runtime environment.';
module.exports.handler = initCommand;