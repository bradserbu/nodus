'use strict';

const NODUS_DIR = '~/.nodus';
const DEFAULT_APP_JSON = './app.json';

// ** Dependencies
const Promise = require('bluebird');
const Action = require('nodus-framework').actions;
const files = require('nodus-framework').files;
const path = require('path');
const write = require('fs').createWriteStream;
const pack = require('tar-pack').pack;
const config = require('../../lib/config');
const logger = require('nodus-framework').logging.createLogger('nodus:pack');

const packageApplication = Action('create-package', options => {

    // Store the current working directory for which the 'nodus' command was run
    const CWD = process.cwd();

    options = options || {};

    // Load Nodus Configuration
    const nodus_config = config();

    // Load app.json
    const appPath = files.resolvePath(options.app || DEFAULT_APP_JSON);

    // Determine what directory to package
    const inputDir = files.dirname(appPath);
    logger.debug('INPUT_DIR', inputDir);

    // Change directory to the location of the app.json file
    logger.debug('Changing directory to app dir...', inputDir);
    process.chdir(inputDir);

    // Load the application json
    logger.debug('Loading application definitions...', appPath);
    const app = files.requireFile(appPath);
    logger.debug('APP', app);

    // TODO: Ensure these properties have a value
    const appName = app.name;
    const appVersion = app.version;

    // TODO: Read the name, version of the package from the app.json
    // const packageName = options.package || 'package.tar.gz';
    const packageName = `${appName}-${appVersion}.package.tar.gz`;
    logger.debug('PACKAGE_NAME', packageName);

    // Determine where to write the package file to
    // const outputDir = files.resolvePath(options.output_dir || process.cwd());
    const outputDir = nodus_config.apps_dir;
    logger.debug('OUTPUT_DIR', outputDir);

    // Package the files in the current working directory
    // TODO: Support logger.info -> write to stdout
    logger.debug('Packaging application...');
    return new Promise((resolve, reject) => {
        pack(inputDir)
            .pipe(write(path.join(outputDir, packageName)))
            .on('error', function (err) {
                logger.debug('ERROR', err);
                reject(err);
            })
            .on('close', function () {
                logger.debug('COMPLETED');
                resolve(packageName);
            });
    });
});

function packageCommand(argv) {
    return packageApplication.run(argv);
}

// ** Exports
module.exports.command = 'pack [options]';
module.exports.desc = 'Package an application.';
module.exports.handler = packageCommand;