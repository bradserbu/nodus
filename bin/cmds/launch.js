'use strict';

// ** Dependencies
const $ = require('highland');
const Promise = require('bluebird');
const Process = require('nodus-framework').processes;
const logger = require('nodus-framework').logging.createLogger('nodus:up');
const init = require('./init').handler;
const pack = require('./pack').handler;
const unpack = require('./unpack').handler;
const build = require('./build').handler;
const execa = require('execa');
const spawn = require('child_process').spawn;

const launchApplication = Process('launch-application')
    .tap('init-app', init)
    .tap('pack-app', pack)
    .tap('unpack-app', unpack)
    .tap('build-app', build)
    .tap('launch-app', options => {
        logger.debug('LAUNCHING Application...');

        // return new Promise((resolve, reject) => {
        const proc = execa('docker-compose', ['up']);

        // proc.stdout.on('data', (data) => {
        //     process.stdout.write(`${data}`);
        // });
        //
        // proc.stderr.on('data', (data) => {
        //     process.stderr.write(`${data}`);
        // });
        //
        // proc.on('close', (code) => {
        //     if (code !== 0) {
        //         logger.debug(`ps process exited with code ${code}`);
        //         // return reject();
        //     }
        //
        //     // resolve();
        // });
        $(proc.stdout)
            .split()
            .each(line => logger.debug('\t', line))
            .done(() => logger.debug("***** DONE *****"));

        return proc
            .catch(err => {
                if (err.signal === 'SIGINT')
                    return logger.debug('Docker event monitor stopped via SIGINT');

                // // Unknown error occurred
                // logger.debug('UNKNOWN_ERROR', 'The docker command failed for an unknown reason', err);
            });
        // });
    });

function launchCommand(argv) {
    return launchApplication
        .run(argv)
        .catch(err => console.error(err));
}

// ** Exports
module.exports.command = 'launch [options]';
module.exports.desc = 'Package, build, and launch an application.';
module.exports.handler = launchCommand;