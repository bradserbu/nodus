#!/usr/bin/env sh

NODE=`which node`

if [ -z "$NODE" ]; then
  echo "NodeJS could not be found on this system."
  echo "Please install NodeJS."
  exit 0;
fi

"$NODE" "bin/nodus.js" "$@"