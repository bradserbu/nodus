/**
 * Service that will emit all docker events
 */
'use strict';

// ** Dependencies
const $ = require('highland');
const monitorEvents = require('./lib/monitor');
const services = require('../../lib/services');

function startMonitor(service) {
    service.logger.debug('Starting Docker Event Monitor...');

    const monitor = monitorEvents();

    // ** Emit all events from the monitor stream
    $(monitor.stream)
        .each(data => {
            service.logger.debug('RECEIVE', data);

            const event = `${data.Action}:${data.Type}`;
            service.emit(event, data);
        });

    service._monitor = monitor;
}

function stopMonitor(service) {
    service.logger.debug('Stopping Docker Event Monitor...');
    service._monitor.close();
}

// ** Exports
module.exports = services('docker-event-service', {
    start: startMonitor,
    stop: stopMonitor
});