'use strict';

// ** Dependencies
const _ = require('lodash');
const execa = require('execa');

/**
 * Get a list of all the containers available on the system
 */
function getContainers() {
    return execa('docker', ['container', 'ls', '-a', "--format", "{{ json . }}"])
        .then(result => {

            // Capture stdout
            const output = result.stdout;

            // Split by lines
            const lines = output.split('\n');

            // Map each line to a json object
            return _.map(lines, line => JSON.parse(line));
        });
}

// ** Exports
module.exports = getContainers;