'use strict';

// ** Dependencies
const $ = require('highland');
const EventEmitter = require('eventemitter2');
const execa = require('execa');
const logger = require('nodus-framework').logging.createLogger('docker-events');

function monitorEvents() {
    const proc = execa('docker', ['events', "--format", "{{ json . }}"]);

    const stream = $(proc.stdout)
        .split()
        .map(line => {
            if (line)
                return JSON.parse(line)
        })
        .compact();

    // ** Track when the process is killed
    proc.catch(err => {
        if (err.signal === 'SIGINT')
            return logger.debug('Docker event monitor stopped via SIGINT');

        // Unknown error occurred
        logger.debug('UNKNOWN_ERROR', 'The docker command failed for an unknown reason', err);
    });

    // Return the results as a stream
    return {
        proc: proc,
        stream: stream,
        close: () => proc.kill('SIGINT')
    };
}

// ** Exports
module.exports = monitorEvents;