'use strict';

// ** Dependencies
const _ = require('lodash');
const util = require('util');
const execa = require('execa');

/**
 * Inspect a docker container
 * @param name
 */
function inspect(id) {
    return execa('docker', ['inspect', id, "--format", "{{ json . }}"])
        .then(result => JSON.parse(result.stdout));
}

// ** Exports
module.exports = inspect;