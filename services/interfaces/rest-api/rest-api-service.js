'use strict';

const SERVER_NAME = 'nodus-server';

// ** Dependencies
const _ = require('lodash');
const Url = require('url');
const Path = require('path');
const services = require('../../../lib/services');
const Action = require('nodus-framework').actions;
const errors = require('nodus-framework').errors;
const functions = require('nodus-framework').functions;
const restify = require('restify');
const enableDestroy = require('server-destroy');
const microtime = require('nodus-framework').microtime;
const WebSocket = require('ws');
const File = require('nodus-framework').files.File;
const mime = require('mime');

const logger = require('nodus-framework').logging.createLogger('rest-api-server');

function SuccessResponse(req, data) {
    return {
        "success": true,
        "request_id": req.id(),
        "data": data
    };
}

function ErrorResponse(req, err) {
    return {
        "success": false,
        "request_id": req.id(),
        "error": {
            "code": err.code,
            "message": err.msg || err.message,
            "data": err.data
            // "stack": err.stack
        }
    };
}

function createApiServer(service) {

    const api = restify.createServer({
        name: SERVER_NAME
    });

    // CORS
    api.use(
        function crossOrigin(req,res,next){
          res.header("Access-Control-Allow-Origin", "*");
          res.header("Access-Control-Allow-Headers", "X-Requested-With");
          return next();
        }
      );      

    api.pre(restify.pre.sanitizePath());
    api.server.setTimeout(60000*20);
    api.use(restify.acceptParser(api.acceptable));
    api.use(restify.dateParser());
    api.use(restify.queryParser());
    api.use(restify.bodyParser());
    api.use(restify.gzipResponse());

    // Timestamp all requests
    // TODO: Restify does not call this if the route is not found
    api.use((req, res, next) => {

        // Add hires timestamp to request
        req.timestamp = microtime.now();

        next();
    });

    // Force Close all current HTTP connections
    enableDestroy(api);

    // Handle 404 Not Found
    api.on('NotFound', (req, res, err, next) => {
        req.timestamp = microtime.now();

        //ErrorResponse(404, req, res, err, next);
        const response = ErrorResponse(req, err);
        res.send(404, response);
        next();
    });

    // Publish 'response' event
    api.on('after', (req, res, route, err) => {

        logger.debug('RUNNING_AFTER_RESPONSE');

        // Use HiRes Timer for requests
        const received = req.time();
        const completed = microtime.now();

        logger.debug('Calculating Elapsed Time...');
        const elapsed = {
            received_at: received,
            started_at: req.timestamp,
            completed_at: completed * .001,
            processing_ms: (completed - req.timestamp) * .001,
            elapsed_ms: completed * .001 - received,
        };

        logger.debug('ELAPSED', elapsed);

        // logger.debug("Emitting 'response' event...");
        // // Emit response event
        // service.emit('response', {
        //     request: {
        //         id: req.id(),
        //         timestamp: req.time(),
        //         method: req.method,
        //         httpVersion: req.httpVersion,
        //         path: req.path(),
        //         url: req.url,
        //         params: req.params,
        //         query: req.query,
        //         contentType: req.contentType(),
        //         contentLength: req.contentLength(),
        //         remoteAddress: req.connection.remoteAddress,
        //         remotePort: req.connection.remotePort,
        //         headers: req.headers
        //     },
        //     response: {
        //         code: res.statusCode,
        //         status: res.statusMessage,
        //         dataLength: res._data
        //             ? res._data.length
        //             : 0,
        //         chunkedEncoding: res.chunkedEncoding,
        //         shouldKeepAlive: res.shouldKeepAlive,
        //         headers: res.getHeaders(),
        //         error: err,
        //     },
        //     elapsed: elapsed
        // });
    });

    // Set api property on service
    service.api = api;

    // Add HealthCheck Endpoint
    // TODO: Move to configuration option
    addEndpoint(service, 'GET', '/', () => service);
}

function createWSServer(service) {

    if (service.wss)
        return service.wss;

    // Create websocket server sharing the same 'http' server as restify
    const wss = new WebSocket.Server({server: service.api});

    logger.debug("WSS", wss);

    // Set WSS property on service
    service.wss = wss;

    return wss;
}

function SubscribeEndpoint(service, path, handler) {

    // Ensure we have a websocket server created
    const wss = createWSServer(service);
    // const action = Action(`ws-request:${path}`, functions.mapNamedArguments(handler));

    // Log All New Connection Requests
    wss.on('connection', function connection(ws, req) {
        // logger.debug("WS_CONNECTION", req);

        const location = Url.parse(req.url, true);
        logger.debug("PATH", location);

        // Match Path to WEBSOCKET Request
        const match_with = Path.join(service.config('basePath'), path);
        logger.debug('MATCHING', {
            requested_path: location.pathname,
            matched_path: match_with
        });

        if (location.pathname === match_with) // TODO: Use proper routing here
        {
            logger.debug("PATH_MATCHED", 'Calling handler...');

            // const params = qs.parse(location);
            const params = location.query;

            const send_event = (event, data) => {
                logger.debug('WS_SEND_EVENT', {event, data});

                try {
                    ws.send(JSON.stringify([event, data]));
                }catch (err) {
                    logger.error('WS_SEND_ERROR', err);
                }
            };

            // Create a new handler to send events
            const event_handler = handler(params, send_event);

            // Call the handler and send a message
            // TODO: Extract Parameters
            // action
            //     .run(params, )
            //     .then(data => {
            //         const response = SuccessResponse(req, data);
            //         ws.send(JSON.stringify(response));
            //         // ws.send(response);
            //     })
            //     .catch(err => {
            //         const response = ErrorResponse(req, err);
            //         ws.send(JSON.stringify(response));
            //     });
            // .finally(() => {
            //     // TODO: Track Request events just like REST requests/responses
            // })
        }
    });

    // Add WS:Upgrade Route
    // service.api.get(path, function upgradeRoute(req, res, next) {
    //     if (!res.claimUpgrade) {
    //         next(errors('UPGRADE_REQUIRED', 'Connection Must Upgrade For WebSockets', {path: path}));
    //         return;
    //     }
    //
    //     const upgrade = res.claimUpgrade();
    //     const shed = ws.accept(req, upgrade.socket, upgrade.head);
    //
    //     const handler_action = Action(handler);
    //
    //     shed.on('text', function (msg) {
    //         logger.info('Received message from websocket client: ' + msg);
    //     });
    //
    //     shed.send('hello there!');
    //
    //     next(false);
    // });

}

function addEndpoint(service, method, path, handler) {
    service.logger.debug("BASE_PATH", service.config("basePath"));

    service.logger.debug("ADD_ENDPOINT", {
        basePath: service.config("basePath"),
        method: method,
        path: path,
        handler: handler
    });

    const api = service.api;
    const route_method = method.toLowerCase();
    const request_handler = Action(`[${method}] ${path}`, functions.mapNamedArguments(handler));

    // ** Check if this is a websocket request method/subscription
    if (route_method === 'subscribe') {
        logger.info("Adding WebSocket Endpoint...", {path: path});
        return SubscribeEndpoint(service, path, handler);
    }

    const route = api[route_method].bind(api);
    if (!route)
        throw errors('NOT_SUPPORTED', 'The method is not supported', {method: method});

    // ** Amend Base Path
    const basePath = service.config('basePath');
    if (basePath)
        path = Path.join(basePath, path);

    logger.debug('PATH', path);

    // Register endpoint
    route(path, (req, res, next) => {

        // Create list of parameters and inject $request, $service, $params
        const params = Object.assign({}, {$request: req, $service: service, $params: req.params}, req.params);

        // Execute the service
        request_handler
            .run(params)
            .then(data => {
                if (data && data.ClassName === 'nodus-framework.files.File') {
                    logger.debug('FILE_RESPONSE', data.filename);

                    // Write contents of file to response stream
                    res.header("Content-Disposition",`inline; filename="${data.filename}"`);
                    res.header("Content-Type", mime.lookup(data.extension));
                    res.header('Cache-Control', 'no-cache, no-store, must-revalidate'); // HTTP 1.1
                    res.header('Pragma', 'no-cache'); // HTTP 1.0
                    res.header('Expires', '0'); // Proxies
                    
                    logger.debug("RESPONSE_HEADERS", res.headers());

                    res.writeHead(200);

                    // Write Contents to stream
                    logger.debug('Writing file contents to stream...');
                    return data
                        .writeStream(res)
                        .then(() => {
                            logger.debug('Finished writing file contents to stream...');
                            res.end();
                            // next();
                        })
                } else {
                    logger.debug('JSON_RESPONSE');
                    const response = SuccessResponse(req, data);
                    res.send(response);
                    next();
                }
            })
            .catch(err => {
                // Emit Server Errors
                const response = ErrorResponse(req, err);
                service.logger.error('REQUEST_ERROR', response);
                res.send(500, response);
                // res.end();
                next();
            })
    });
}

function configure(service, options) {

    // ** Set configuration for baseUrl
    if (options.basePath)
        service.config("basePath", options.basePath);

    if (options.port)
        service.config("port", options.port);

    const endpoints = options.endpoints;

    service.logger.debug('ADD_ENDPOINTS', endpoints);
    _.forEach(endpoints, endpoint => service
        .command('add-endpoint')
        .run(endpoint.method, endpoint.path, endpoint.handler)
    );
}

function startListener(service) {

    service.logger.debug('Starting HTTP Listener...', {port: service.config("port")});
    const port = service.config('port');
    const api = service.api;

    api.listen(port);
}

function stopListener(service) {

    service.logger.debug('Stopping HTTP Listener...');
    const api = service.api;
    api.destroy();
}

// ** Exports
module.exports = services('rest-api', {
    description: 'Nodus Service to respond to REST based api requests.',
    config: {
        port: {
            type: 'int',
            required: true,
            defaultValue: 8080
        },
        basePath: {
            type: 'string',
            required: true,
            defaultValue: '/'
        }
    },
    commands: {
        'add-endpoint': {
            provider: addEndpoint,
            description: 'Add an API Endpoint to respond to requests',
            parameters: {
                method: {
                    description: 'The HTTP Method for the endpoint',
                    type: "enum:GET|POST|PUT|PATCH|DELETE",
                    required: true
                },
                path: {
                    description: 'The URL path for the endpoint',
                    type: 'string',
                    required: true
                },
                handler: {
                    description: 'The action to run to respond to requests',
                    required: true
                }
            }
        }
    },
    init: createApiServer,
    configure: configure,
    start: startListener,
    stop: stopListener
});